
public class binaryOps {
	int codeSize = 2;
	binaryOps(int cs) {
		codeSize = cs;
	}
	byte[] bytesFromInteger(int in) {	 
		  int m=codeSize;
		  byte[] result = new byte[m];
		  if(m >= 4) result[m - 4] = (byte) (in >> 24);
		  if(m >= 3) result[m - 3] = (byte) (in >> 16);
		  if(m >= 2) result[m - 2] = (byte) (in >> 8);
		  result[m - 1] = (byte) (in);
		  return result;
	}
	byte[] unsignedBytesFromInteger(int in) {
		byte[] tempval = bytesFromInteger(in);
		byte[] retval = new byte[tempval.length];
		for(int i=0; i < retval.length; i++) {
			retval[i] = byteFromTwosComp(tempval[i]);
		}
		return retval;
	}
	byte[] bytesFromBooleans(boolean[] in) {
		return bytesFromInteger(integerFromBooleans(in));
	}
	boolean[] truncateBooleanFromFront(boolean[] in, int size) {
		boolean retval[] = new boolean[size];
		for(int i=0; i < size; i++) {
			retval[i] = in[i];
		}
		return retval;
	}
	int integerFromBytes(byte[] in) {
		int retval = 0;
		int val[] = new int[in.length];
		for(int i=0; i < in.length; i++) {
			if(in[in.length - i - 1] < 0) {
				in[in.length - i - 1] = byteFromTwosComp(in[in.length - i - 1]);
				val[in.length - 1 - i] = (int)in[in.length - 1 - i];
				val[in.length - i - 1] = val[in.length - i - 1] << (8 * i);
				retval += val[in.length - i - 1];
				int tval = 1;
				tval = tval << ((8*(i+1)) - 1);
				retval += tval;
				tval = 0;
			}
			else {
				val[in.length - i - 1] = in[in.length - i - 1];
				val[in.length - i - 1] = val[in.length - i - 1] << (8 * i);
				retval += val[in.length - 1 - i];
				int a = 0;
				a++;
			}
		}
		return retval;
	}
	int integerFromBooleans(boolean[] in) {
		int retval = 0;
		for(int i=0; i < in.length; i++) {
			int tempval = 0;
			if(in[in.length - 1 - i]) {
				tempval = 1;
				tempval = tempval << (i);
			}
			retval += tempval;
		}
		return retval;
	}
	boolean[] booleanFromInteger(int in) {
		boolean tempret[] = new boolean[32];
		int count=0;
		while(in != 0) {
			if(in % 2 == 0) tempret[31 - count] = false;
			else tempret[31 - count] = true;
			in = in >> 1;
			count++;
		}
		boolean realRet[] = new boolean[count];
		for(int i=0; i < count; i++) {
			realRet[count - 1 - i] = tempret[32 - 1 - i];
		}
		return realRet;
	}
	boolean[] booleanFromBytes(byte[] in) {
		return booleanFromInteger(integerFromBytes(in));
	}
	int integerRemoveLeading1(int in) {
		boolean bits[] = booleanFromInteger(in);
		int retval = integerFromBooleans(booleanRemoveLeading1(bits));
		return retval;
	}
	int integerAddLeading1(int in) {
		boolean bits[] = booleanFromInteger(in);
		int retval = integerFromBooleans(booleanAddLeading1(bits));
		return retval;
	}
	boolean[] booleanAddLeading1(boolean[] bits) {
		
		boolean bits2[] = new boolean[bits.length + 1];
		bits2[0] = true;
		for(int i=1; i < bits2.length; i++) {
			bits2[i] = bits[i - 1];
		}
		return bits2;
	}
	boolean[] booleanRemoveLeading1(boolean[] bits) {
		bits[0] = false;
		boolean bits2[] = new boolean[bits.length - 1];
		for(int i=0; i < bits.length - 1; i++) {
			bits2[i] = bits[i+1];
		}
		return bits2;
	}
	int integerFromTwosComp(int in) {
		if(in < 0) return in + 128;
		return in;
	}
	int integerToTwosComp(int in) {
		return in + 128;
	}
	byte byteFromTwosComp(byte in) {
		if(in < 0) in = (byte)(in + 128);
		return in;
	}
	byte byteToTwosComp(byte in) {
		in = (byte)(~in);
		in++;
		return in;
	}
	boolean[] booleanToTwosComp(boolean[] in) {
		boolean retval[] = new boolean[in.length];
		for(int i=0; i < in.length; i++) {
			retval[i] = !in[i];
		}
		int p = 0;
		while(retval[p] == true) p++;
		retval[p] = true;
		return retval;
	}
	boolean[] makeByteInc(boolean[] in, int numBytes) {
		boolean retval[] = new boolean[(numBytes * 8)];
		int count = 0;
		int startPoint = numBytes * 8;
		for(int i=0; i < (numBytes*8); i++) {
			if(i >= (startPoint - in.length)) {
				retval[i] = in[count];
				count++;
			}
			else {
				retval[i] = false;
			}
		}
		return retval;
	}
	long startTime,taskTime;
	String timerId;
	void startTimer(String timerName) {
		startTime = System.currentTimeMillis();
		timerId = timerName;
	}
	long endTimer() {
		taskTime = System.currentTimeMillis() - startTime;
		System.out.println(timerId + " took: " + taskTime + " ms");
		return taskTime;
	}
}
