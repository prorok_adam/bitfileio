
public class q {
	int size = 0;
	TreeNode first, last;
	TreeNode[] ret = new TreeNode[2];
	q() {
		first = null;
		last = first;
	}
	void add(TreeNode a ) {
		if(first == null) {
			first = a;
			last = a;
			first.qnext = last;
			first.qprev = null;
			last.qnext = null;
			last.qprev = first;
			size++;
			return;
			
		}
		if(a.freq >= last.freq) {
			last.qnext = a;
			a.qprev = last;
			last = a;
			last.qnext = null;
			size++;
			
			return;
		}
		TreeNode temp = first;
		while((temp.qnext != null) && (temp.freq < a.freq)){
				temp = temp.qnext;
		}
		if(temp == last) {
			last.qnext = a;
			a.qprev = last;
			last = a;
			last.qnext = null;
		}
		else {
			a.qnext = temp.qnext;
			a.qprev = temp;
			temp.qnext = a;
			a.qnext.qprev = a;
			
		}
		size++;
		
	}
	TreeNode[] popLowest2() {
		ret[0] = first;
		ret[1] = first.qnext;
		first = first.qnext.qnext;
		if(first == null) first = last;
		first.qprev = null;
		size = size -2;
		return ret;
	}
}
